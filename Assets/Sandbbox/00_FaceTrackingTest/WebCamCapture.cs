﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

namespace traksy
{
    public class WebCamCapture : MonoBehaviour
    {

        private WebCamTexture _texture;

        private RawImage _rawimage;
        private RawImage RawImage
        {
            get
            {
                if (_rawimage == null) { _rawimage = GetComponent<RawImage>(); }

                return _rawimage;
            }
        }

        [SerializeField]
        private Material _mateiral = default;

        [SerializeField]
        private VisualEffect _vfx = default;

        private void OnDisable()
        {
            StopCamera();
        }

        private void Start()
        {
            StartInCamera();
        }

        private void StopCamera()
        {
            if (_texture == null) { return; }

            _texture.Stop();
            _texture = null;
        }

        public void StartInCamera()
        {
            if (gameObject.activeInHierarchy == false) { return; }

            if (_texture != null) { StopCamera(); }

            var devices = WebCamTexture.devices;

            if (devices.Length < 1) { return; }

            foreach (var cam in devices)
            {
                if (cam.isFrontFacing)
                {
                    Debug.Log($"name = {cam.name}");

                    if (cam.name.Contains("TrueDepth")) { continue; }

                    _texture = new WebCamTexture(cam.name, 1280, 720, 30);
                }
            }

            if (_texture == null) { return; }

            RawImage.texture = _texture;
            RawImage.transform.localScale = Vector3.one;
            _mateiral.mainTexture = _texture;
            _vfx.SetTexture("CameraTexture", _texture);
            _texture.Play();
        }

        public void StartOutCamera()
        {
            if (gameObject.activeInHierarchy == false) { return; }

            if (_texture != null) { StopCamera(); }

            var devices = WebCamTexture.devices;

            if (devices.Length < 1) { return; }

            foreach (var cam in devices)
            {
                if (cam.isFrontFacing == false)
                {
                    Debug.Log($"name = {cam.name}");

                    if (cam.name.Contains("Telephoto") || cam.name.Contains("Dual")) { continue; }

                    _texture = new WebCamTexture(cam.name, 1280, 720, 30);
                }
            }

            if (_texture == null) { return; }

            RawImage.texture = _texture;
            RawImage.transform.localScale = new Vector3(1, -1, 1);
            _texture.Play();
        }

        public void Zoom(float zoom)
        {
            var scale = RawImage.transform.localScale;

            if (scale.y > 0)
            {
                RawImage.transform.localScale = Vector3.one + Vector3.one * zoom;
            }
            else
            {
                RawImage.transform.localScale = new Vector3(1, -1, 1) + new Vector3(1, -1, 1) * zoom;
            }
        }

    }
}
