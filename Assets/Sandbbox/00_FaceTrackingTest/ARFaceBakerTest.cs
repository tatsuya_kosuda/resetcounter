﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.VFX;

public class ARFaceBakerTest : MonoBehaviour
{

    private ARFace _arFace;

    [SerializeField]
    private ComputeShader _cs = null;

    private ComputeBuffer _pBuffer, _cBuffer;

    private List<Vector3> _pList, _cList;

    [SerializeField]
    private RenderTexture _pMap = null, _cMap = null;

    private RenderTexture _tmpPMap, _tmpCMap;

    [SerializeField]
    private Camera _arCamera = null;

    [SerializeField]
    private ARCameraManager _arCameraManager = null;

    private Texture2D _cameraImage, _rotatedImage;

    [SerializeField]
    private RawImage _checkImage = null;

    [SerializeField]
    private VisualEffect _vfx = null;

    [SerializeField]
    private Material _material = null;

    private void Awake()
    {
        _arFace = GetComponent<ARFace>();
    }

    private void OnEnable()
    {
        _arFace.updated += OnARFaceUpdated;
        _arCameraManager.frameReceived += OnARCameraFrameReceived;
    }

    private void OnDisable()
    {
        _arFace.updated -= OnARFaceUpdated;
        _arCameraManager.frameReceived -= OnARCameraFrameReceived;
    }

    private void OnARFaceUpdated(ARFaceUpdatedEventArgs e)
    {
        if (e.face.trackingState != TrackingState.Tracking ||
            e.face.indices.IsCreated == false ||
            e.face.vertices.IsCreated == false ||
            _rotatedImage == null) { return; }

        // [PREPARE] bake vertices
        // _pList = e.face.vertices.ToList();
        var mesh = new Mesh();
        mesh.SetVertices(e.face.vertices);
        mesh.SetIndices(e.face.indices, MeshTopology.Triangles, 0, false);
        _pList = GetIncreasePositions(mesh, 10000);

        if (_pList == null || _pList.Count <= 0) { return; }

        for (int i = 0; i < _pList.Count; i++)
        {
            _pList[i] = transform.localToWorldMatrix.MultiplyPoint(_pList[i]);
        }

        // [PREPARE] bake colors
        if (_cList == null) { _cList = new List<Vector3>(); }

        _cList.Clear();

        for (int i = 0; i < _pList.Count; i++)
        {
            var vp = _arCamera.WorldToViewportPoint(_pList[i]);
            var c = _rotatedImage.GetPixelBilinear(vp.x, vp.y).linear;
            _cList.Add(new Vector3(c.r, c.g, c.b));
        }

        TransferToComputeShader();
        _pBuffer.Dispose();
        _cBuffer.Dispose();
        _pBuffer = null;
        _cBuffer = null;
    }

    private void OnARCameraFrameReceived(ARCameraFrameEventArgs e)
    {
        if (_arCameraManager.TryAcquireLatestCpuImage(out var cameraImage))
        {
            if (_cameraImage == null) { _cameraImage = new Texture2D(cameraImage.width, cameraImage.height, TextureFormat.RGBA32, false); }

            XRCpuImage.ConversionParams param = new XRCpuImage.ConversionParams(cameraImage, TextureFormat.RGBA32, XRCpuImage.Transformation.MirrorX);
            var rawTextureData = _cameraImage.GetRawTextureData<byte>();

            try
            {
                unsafe
                {
                    cameraImage.Convert(param, new System.IntPtr(rawTextureData.GetUnsafePtr()), rawTextureData.Length);
                }
            }
            finally
            {
                cameraImage.Dispose();
            }

            _cameraImage.Apply();
            RotateTexture();
            _checkImage.texture = _rotatedImage;
            _material.mainTexture = _rotatedImage;
            _vfx.SetTexture("CameraTexture", _rotatedImage);
        }
    }

    private void TransferToComputeShader()
    {
        int mw = _pMap.width, mh = _pMap.height;
        int vcount = _pList.Count;
        int vcountX3 = vcount * 3;

        if (_pBuffer == null) { _pBuffer = new ComputeBuffer(vcountX3, sizeof(float)); }

        if (_cBuffer == null) { _cBuffer = new ComputeBuffer(vcountX3, sizeof(float)); }

        if (_tmpPMap == null)
        {
            _tmpPMap = new RenderTexture(mw, mh, 0, _pMap.format);
            _tmpPMap.enableRandomWrite = true;
            _tmpPMap.Create();
        }

        if (_tmpCMap == null)
        {
            _tmpCMap = new RenderTexture(mw, mh, 0, _cMap.format);
            _tmpCMap.enableRandomWrite = true;
            _tmpCMap.Create();
        }

        _cs.SetInt("VertexCount", vcount);
        _pBuffer.SetData(_pList);
        _cBuffer.SetData(_cList);
        _cs.SetBuffer(0, "PositionBuffer", _pBuffer);
        _cs.SetTexture(0, "PositionMap", _tmpPMap);
        _cs.SetBuffer(0, "ColorBuffer", _cBuffer);
        _cs.SetTexture(0, "ColorMap", _tmpCMap);
        _cs.Dispatch(0, mw / 8, mh / 8, 1);
        Graphics.CopyTexture(_tmpPMap, _pMap);
        Graphics.CopyTexture(_tmpCMap, _cMap);
    }

    private List<Vector3> GetIncreasePositions(Mesh mesh, float pcountPerArea = 1f)
    {
        var res = new List<Vector3>();
        var tris = mesh.triangles;
        var verts = new List<Vector3>();
        mesh.GetVertices(verts);

        for (int i = 0; i < tris.Length; i += 3)
        {
            int idx0 = tris[i];
            int idx1 = tris[i + 1];
            int idx2 = tris[i + 2];
            Vector3 pos0 = verts[idx0];
            Vector3 pos1 = verts[idx1];
            Vector3 pos2 = verts[idx2];
            var area = Vector3.Cross(pos1 - pos0, pos2 - pos0).magnitude * 0.5f;
            var pointnum = Mathf.CeilToInt(area * pcountPerArea);

            for (int pIdx = 0; pIdx < pointnum; pIdx++)
            {
                float wait0 = Random.value;
                float wait1 = Random.value;

                if (wait0 + wait1 > 1)
                {
                    wait0 = 1 - wait0;
                    wait1 = 1 - wait1;
                }

                float wait2 = 1 - wait0 - wait1;
                Vector3 pos = pos0 * wait0 + pos1 * wait1 + pos2 * wait2;
                res.Add(pos);
            }
        }

        return res;
    }

    private void RotateTexture()
    {
        var pixels = _cameraImage.GetPixels32();
        pixels = RotateMatrix(pixels, _cameraImage.width, _cameraImage.height);

        if (_rotatedImage == null) { _rotatedImage = new Texture2D(_cameraImage.height, _cameraImage.width, _cameraImage.format, false); }

        _rotatedImage.SetPixels32(pixels);
        _rotatedImage.Apply();
        //Color32[] original = _cameraImage.GetPixels32();
        //Color32[] rotated = new Color32[original.Length];
        //int w = _cameraImage.width;
        //int h = _cameraImage.height;
        //bool clockwise = true;
        //int iRotated, iOriginal;
        //for (int j = 0; j < h; ++j)
        //{
        //    for (int i = 0; i < w; ++i)
        //    {
        //        iRotated = (i + 1) * h - j - 1;
        //        iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
        //        rotated[iRotated] = original[iOriginal];
        //    }
        //}
        //if (_rotatedImage == null) { _rotatedImage = new Texture2D(h, w, _cameraImage.format, false); }
        //_rotatedImage.SetPixels32(rotated);
        //_rotatedImage.Apply();
    }

    private Color32[] RotateMatrix(Color32[] matrix, int w, int h)
    {
        Color32[] ret = new Color32[w * h];

        for (int i = 0; i < h; ++i)
        {
            for (int j = 0; j < w; ++j)
            {
                ret[(h - 1) - i + j * h] = matrix[j + i * w];
            }
        }

        return ret;
    }
}
