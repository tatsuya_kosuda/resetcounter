﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using resetcounter;

public class CSVTest : MonoBehaviour
{

    private List<LocalizeText> _testData = new List<LocalizeText>();

    [SerializeField]
    private Text _jaText = default;

    private void Start()
    {
        ReadCSV();
    }

    private void ReadCSV()
    {
        var csvFile = Resources.Load("test") as TextAsset;

        using (var reader = new StringReader(csvFile.text))
        {
            // skip 3 line
            reader.ReadLine();
            reader.ReadLine();
            reader.ReadLine();

            while (reader.Peek() > -1)
            {
                var lineData = reader.ReadLine();
                var data = new LocalizeText(lineData.Split(','));
                _testData.Add(data);
            }
        }

        foreach (var data in _testData)
        {
            Debug.Log(data.id + data.ja + data.en + data.zhcn + data.zhtw + data.th);
        }

        _jaText.text = _testData[0].ja.Replace("\\n", "\n");
    }

}
