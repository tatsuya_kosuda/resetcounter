﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AromaShooterTest : MonoBehaviour
{

    public void OnTapButton()
    {
        UnityAromaShooter.OpenConnectionView();
    }

    public void Diffuse()
    {
        UnityAromaShooter.Diffuse(3000);
    }

}
