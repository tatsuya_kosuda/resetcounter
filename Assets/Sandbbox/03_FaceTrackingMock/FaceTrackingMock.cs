﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.VFX;
using klib;

using DG.Tweening;

public class FaceTrackingMock : MonoBehaviour
{

    private ARFace _arFace;

    private bool _isDetectedFace;

    [SerializeField]
    private Text _debugText = default;

    [SerializeField]
    private Button _visualizeButton = default;

    [SerializeField]
    private ComputeShader _cs = null;

    private ComputeBuffer _pBuffer, _uvBuffer;

    private List<Vector3> _pList;
    private List<Vector2> _uvList;

    [SerializeField]
    private RenderTexture _pMap = null, _uvMap = null;

    private RenderTexture _tmpPMap, _tmpUVMap;

    [SerializeField]
    private Camera _arCamera = null;

    private bool _isAnimated;

    [SerializeField]
    private VisualEffect _meshVFX = default, _arFAceVFX = default;

    private bool _isFaceUpdatable = false;

    [SerializeField]
    private Material _vfxRenderTextureMat = default;

    private void Awake()
    {
        _arFace = GetComponent<ARFace>();
        _isFaceUpdatable = true;
    }

    private void OnEnable()
    {
        _arFace.updated += OnARFaceUpdated;
        _visualizeButton.onClick.AddListener(OnClickVisualizeButton);
        //_isAnimated = false;
        //_vfxRenderTextureMat.SetFloat("_Alpha", -1);
        //_meshVFX.SetFloat("Noise", 0);
        //_arFAceVFX.SetFloat("Alpha", 0);
        //_arFAceVFX.SetFloat("Noise", 0.1f);
        //_arFAceVFX.SetFloat("ColorAnimation", 0f);
        //_isFaceUpdatable = true;
    }

    private void OnDisable()
    {
        _arFace.updated -= OnARFaceUpdated;
        _visualizeButton.onClick.RemoveAllListeners();
        _vfxRenderTextureMat.SetFloat("_Alpha", -1);
    }

    private void Update()
    {
        _debugText.text = $"IsDetectedFace = {_isDetectedFace}";
    }

    private void Start()
    {
        //StartCoroutine(MockSequence());
    }

    private IEnumerator MockSequence()
    {
        yield return null;
        // 顔が検知されるまで待つ
        yield return new WaitUntil(() => _isDetectedFace);
    }

    private void OnARFaceUpdated(ARFaceUpdatedEventArgs e)
    {
        _isDetectedFace = e.face.trackingState == TrackingState.Tracking;

        if (e.face.trackingState != TrackingState.Tracking ||
            e.face.indices.IsCreated == false ||
            e.face.vertices.IsCreated == false ||
            _isFaceUpdatable == false) { return; }

        // [PREPARE] bake vertices
        // _pList = e.face.vertices.ToList();
        var mesh = new Mesh();
        mesh.SetVertices(e.face.vertices);
        mesh.SetIndices(e.face.indices, MeshTopology.Triangles, 0, false);
        _pList = GetIncreasePositions(mesh, 10);

        if (_pList == null || _pList.Count <= 0) { return; }

        for (int i = 0; i < _pList.Count; i++)
        {
            _pList[i] = transform.localToWorldMatrix.MultiplyPoint(_pList[i]);
        }

        // [PREPARE] bake colors
        if (_uvList == null) { _uvList = new List<Vector2>(); }

        _uvList.Clear();

        for (int i = 0; i < _pList.Count; i++)
        {
            var vp = _arCamera.WorldToViewportPoint(_pList[i]);
            _uvList.Add(new Vector2(vp.x, vp.y));
        }

        TransferToComputeShader();
        _pBuffer.Dispose();
        _uvBuffer.Dispose();
        _pBuffer = null;
        _uvBuffer = null;
    }

    private void TransferToComputeShader()
    {
        int mw = _pMap.width, mh = _pMap.height;
        int vcount = _pList.Count;
        int vcountX3 = vcount * 3;
        int vcountX2 = vcount * 2;

        if (_pBuffer == null) { _pBuffer = new ComputeBuffer(vcountX3, sizeof(float)); }

        if (_uvBuffer == null) { _uvBuffer = new ComputeBuffer(vcountX2, sizeof(float)); }

        if (_tmpPMap == null)
        {
            _tmpPMap = new RenderTexture(mw, mh, 0, _pMap.format);
            _tmpPMap.enableRandomWrite = true;
            _tmpPMap.Create();
        }

        if (_tmpUVMap == null)
        {
            _tmpUVMap = new RenderTexture(mw, mh, 0, _uvMap.format);
            _tmpUVMap.enableRandomWrite = true;
            _tmpUVMap.Create();
        }

        _cs.SetInt("VertexCount", vcount);
        _pBuffer.SetData(_pList);
        _uvBuffer.SetData(_uvList);
        _cs.SetBuffer(0, "PositionBuffer", _pBuffer);
        _cs.SetTexture(0, "PositionMap", _tmpPMap);
        _cs.SetBuffer(0, "UVBuffer", _uvBuffer);
        _cs.SetTexture(0, "UVMap", _tmpUVMap);
        _cs.Dispatch(0, mw / 8, mh / 8, 1);
        Graphics.CopyTexture(_tmpPMap, _pMap);
        Graphics.CopyTexture(_tmpUVMap, _uvMap);
    }

    private void OnClickVisualizeButton()
    {
        if (DOTween.IsTweening(this)) { DOTween.Kill(this); }

        if (_isAnimated)
        {
            Debug.Log("init");
            // init
            _isAnimated = false;
            _vfxRenderTextureMat.SetFloat("_Alpha", -1);
            _meshVFX.SetFloat("Noise", 0);
            _arFAceVFX.SetFloat("Alpha", 0);
            _arFAceVFX.SetFloat("Noise", 0.03f);
            _arFAceVFX.SetFloat("ColorAnimation", 0f);
            _isFaceUpdatable = true;
        }
        else
        {
            Debug.Log("animate");
            // animation
            _isAnimated = true;
            var seq = DOTween.Sequence();
            seq
            .Insert(0f, DOTween.To(() => _vfxRenderTextureMat.GetFloat("_Alpha"), a =>
            {
                _vfxRenderTextureMat.SetFloat("_Alpha", a);
            }, 1, 3).SetEase(Ease.Linear))
            //.Insert(3f, DOTween.To(() => _meshVFX.GetFloat("Noise"), n =>
            //{
            //    _meshVFX.SetFloat("Noise", n);
            //}, 1, 1))
            .InsertCallback(3f, () => { _meshVFX.SetFloat("Noise", 1); })
            .Insert(3f, DOTween.To(() => _arFAceVFX.GetFloat("Alpha"), a =>
            {
                _arFAceVFX.SetFloat("Alpha", a);
            }, 1, 2f))
            .Insert(4f, DOTween.To(() => _arFAceVFX.GetFloat("Noise"), n =>
            {
                _arFAceVFX.SetFloat("Noise", n);
            }, 0, 2).SetEase(Ease.InOutQuad))
            .InsertCallback(7f, () => { _isFaceUpdatable = false; })
            .Insert(7f, DOTween.To(() => _arFAceVFX.GetFloat("Noise"), n =>
            {
                _arFAceVFX.SetFloat("Noise", n);
            }, 0.1f, 3).SetEase(Ease.OutCirc))
            .Insert(7f, DOTween.To(() => _arFAceVFX.GetFloat("ColorAnimation"), c =>
            {
                _arFAceVFX.SetFloat("ColorAnimation", c);
            }, 1, 1f))
            .Insert(9f, DOTween.To(() => _arFAceVFX.GetFloat("Alpha"), a =>
            {
                _arFAceVFX.SetFloat("Alpha", a);
            }, 0f, 1).SetEase(Ease.InQuad))
            .SetTarget(this);
        }
    }

    private List<Vector3> GetIncreasePositions(Mesh mesh, float pcountPerArea = 1f)
    {
        var res = new List<Vector3>();
        var tris = mesh.triangles;
        var verts = new List<Vector3>();
        mesh.GetVertices(verts);

        for (int i = 0; i < tris.Length; i += 3)
        {
            int idx0 = tris[i];
            int idx1 = tris[i + 1];
            int idx2 = tris[i + 2];
            Vector3 pos0 = verts[idx0];
            Vector3 pos1 = verts[idx1];
            Vector3 pos2 = verts[idx2];
            var area = Vector3.Cross(pos1 - pos0, pos2 - pos0).magnitude * 0.5f;
            var pointnum = Mathf.CeilToInt(area * pcountPerArea);

            for (int pIdx = 0; pIdx < pointnum; pIdx++)
            {
                //float wait0 = Random.value;
                float wait0 = Mathf.PerlinNoise((idx0 + pIdx) * 100, (idx1 + pIdx) * 100);
                //float wait1 = Random.value;
                float wait1 = Mathf.PerlinNoise((idx1 + pIdx) * 100, (idx2 + pIdx) * 100);

                if (wait0 + wait1 > 1)
                {
                    wait0 = 1 - wait0;
                    wait1 = 1 - wait1;
                }

                float wait2 = 1 - wait0 - wait1;
                Vector3 pos = pos0 * wait0 + pos1 * wait1 + pos2 * wait2;
                res.Add(pos);
            }
        }

        return res;
    }

    public void SavePositionMap()
    {
        Debug.Log("SAVE");
        Debug.Log($"p = {_arCamera.transform.position} r = {_arCamera.transform.eulerAngles}");
        Debug.Log($"p.x = {_arCamera.transform.position.x} p.y = {_arCamera.transform.position.y} p.z = {_arCamera.transform.position.z}");
        Texture2D tex = new Texture2D(_pMap.width, _pMap.height, TextureFormat.RGBA32, false);
        tex.filterMode = FilterMode.Point;
        RenderTexture.active = _pMap;
        tex.ReadPixels(new Rect(0, 0, _pMap.width, _pMap.height), 0, 0);
        tex.Apply();
        // Encode texture into PNG
        byte[] bytes = tex.EncodeToPNG();

        if (!System.IO.Directory.Exists(Application.persistentDataPath + "/Textures"))
        {
            System.IO.Directory.CreateDirectory(Application.persistentDataPath + "/Textures");
        }

        //Write to a file in the project folder
        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/Textures/PositionMap.png", bytes);
        Destroy(tex);
    }

}
