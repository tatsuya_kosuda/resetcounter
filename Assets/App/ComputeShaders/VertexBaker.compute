﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CSMain

uint VertexCount;

StructuredBuffer<float> PositionBuffer;
StructuredBuffer<float> UVBuffer;

RWTexture2D<float4> PositionMap;
RWTexture2D<float4> UVMap;

// Hash function from H. Schechter & R. Bridson, goo.gl/RXiKaH
uint Hash(uint s)
{
    s ^= 2747636419u;
    s *= 2654435769u;
    s ^= s >> 16;
    s *= 2654435769u;
    s ^= s >> 16;
    s *= 2654435769u;
    return s;
}

[numthreads(8,8,1)]
void CSMain (uint2 id : SV_DispatchThreadID)
{
    //uint i = Hash(id.x + id.y * 65536) % VertexCount;
    uint i = (id.x + id.y * 256) % VertexCount;
    float3 p = float3(
        PositionBuffer[i * 3],
        PositionBuffer[i * 3 + 1],
        PositionBuffer[i * 3 + 2]
    );
    float2 uv = float2(
        UVBuffer[i * 2],
        UVBuffer[i * 2 + 1]
    );
    PositionMap[id] = float4(p, 1);
    UVMap[id] = float4(uv, 1, 1);
}
