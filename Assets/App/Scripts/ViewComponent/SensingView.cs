using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using klib;

namespace resetcounter
{
    public class SensingView : MonoBehaviour
    {

        [SerializeField]
        private RawImage _arCameraRawImage = default, _vfxCameraRawImage = default;

        [SerializeField]
        private CanvasGroup _rootCanvasGroup = default;

        [SerializeField]
        private Material _vfxCameraRawImageMaterial = default, _blackMaterial = default;

        private static readonly int ALPHA_ID = Shader.PropertyToID("_Alpha");

        [SerializeField]
        private Image _line = default;

        [SerializeField]
        private TextMeshProUGUI _percentageText = default;

        private int _percentage = 0;

        [SerializeField]
        private TextMeshProUGUI _scanningText = default;

        private bool _scanningComplete;

        [SerializeField]
        private CanvasGroup _percentageCanvasGroup = default, _leahCanvasGroup = default;

        [SerializeField]
        private Image _black = default;

        [SerializeField]
        private RectTransform _leahLineRectTR = default, _leahDefaultRectTR = default;

        [SerializeField]
        private TMP_FontAsset[] _fonts = default;

        [SerializeField]
        private Image _gaugeBg = default;

        private void OnDisable()
        {
            _vfxCameraRawImageMaterial.SetFloat(ALPHA_ID, -1);
            _blackMaterial.SetFloat(ALPHA_ID, -1);
            _percentage = 0;
            _line.rectTransform.SetAnchoredPosX(-15);
            _percentageText.text = "0";
            _scanningText.text = "";
            _scanningComplete = false;
            _percentageCanvasGroup.alpha = 0;
            _black.color = Color.black;
            _leahCanvasGroup.alpha = 1;
            _leahLineRectTR.localScale = Vector3.zero;
            _leahDefaultRectTR.localScale = Vector3.zero;
            _gaugeBg.SetAlpha(0f);
        }

        public void Setup(Texture arCameraRT, Texture vfxCameraRT)
        {
            _arCameraRawImage.texture = arCameraRT;
            _vfxCameraRawImage.texture = vfxCameraRT;
        }

        public void Show()
        {
            _rootCanvasGroup.DOFade(1f, .3f);
            _leahDefaultRectTR.DOScale(1f, .6f);
        }

        public void Hide(System.Action onComplete)
        {
            _black.color = Color.white;
            _rootCanvasGroup.DOFade(0f, .3f).OnComplete(() =>
            {
                onComplete?.Invoke();
                gameObject.SetActive(false);
            });
        }

        public Sequence BeforeVisualize(DiagnoticsResult result)
        {
            if (DOTween.IsTweening(_leahDefaultRectTR)) { _leahDefaultRectTR.DOKill(); }

            StartCoroutine(ScanningTextAnimationEnumerator());
            var seq = DOTween.Sequence();
            seq
            .Insert(0f, _line.rectTransform.DOAnchorPosX(2048 - 15, 3f).SetEase(Ease.Linear))
            .Insert(0f, DOTween.To(() => _percentage, p =>
            {
                _percentage = p;
                _percentageText.text = _percentage.ToString();
            }, 100, 3).SetEase(Ease.Linear))
            .Insert(0f, _leahDefaultRectTR.DOScale(0f, .6f))
            .Insert(0f, _leahLineRectTR.DOScale(1f, .6f))
            .Insert(0f, _percentageCanvasGroup.DOFade(1f, 0.3f))
            .InsertCallback(0f, () =>
            {
                AudioManager.Instance.PlaySE("14_InspirationGATE_D");
            })
            .InsertCallback(2.8f, () =>
            {
                AudioManager.Instance.StopSE("14_InspirationGATE_D", true, 1f);
            })
            .InsertCallback(3f, () =>
            {
                AudioManager.Instance.PlaySE("09_Line_in_B_Reved");
            })
            .InsertCallback(3f, () => _scanningComplete = true)
            .Insert(2.9f, _leahDefaultRectTR.DOScale(1f, .6f))
            .Insert(2.9f, _leahLineRectTR.DOScale(0f, .6f))
            .Insert(5f, DOTween.To(() => _vfxCameraRawImageMaterial.GetFloat(ALPHA_ID), a =>
            {
                _vfxCameraRawImageMaterial.SetFloat(ALPHA_ID, a);
            }, 1, 1).SetEase(Ease.Linear))
            .InsertCallback(7f, () =>
            {
                AudioManager.Instance.PlaySE(result.ToString());
            })
            .Insert(5f, DOTween.To(() => _blackMaterial.GetFloat(ALPHA_ID), a =>
            {
                _blackMaterial.SetFloat(ALPHA_ID, a);
            }, 1, 1).SetEase(Ease.Linear))
            .Insert(10f, _gaugeBg.DOFade(1f, 1f).SetEase(Ease.Linear));
            return seq;
        }

        private IEnumerator ScanningTextAnimationEnumerator()
        {
            var info = LocalizationUtils.GetText("5-1");
            _scanningText.font = _fonts[(int)LocalizationUtils.lang];
            _scanningText.text = info.text;
            int loop = 0;

            while (_scanningComplete == false)
            {
                yield return new WaitForSeconds(.5f);
                loop++;

                switch (loop % 3)
                {
                    case 1:
                        _scanningText.text = info.text + ".";
                        break;

                    case 2:
                        _scanningText.text = info.text + "..";
                        break;

                    case 0:
                        _scanningText.text = info.text + "...";
                        break;
                }
            }

            info = LocalizationUtils.GetText("5-2");
            _scanningText.text = info.text;
            var seq = DOTween.Sequence();
            seq
            .AppendCallback(() => _scanningText.SetActive(false))
            .AppendInterval(0.1f)
            .AppendCallback(() => _scanningText.SetActive(true))
            .AppendInterval(0.1f)
            .AppendCallback(() => _scanningText.SetActive(false))
            .AppendInterval(0.1f)
            .AppendCallback(() => _scanningText.SetActive(true))
            .AppendInterval(0.1f)
            .AppendCallback(() => _scanningText.SetActive(false))
            .AppendInterval(0.1f)
            .AppendCallback(() => _scanningText.SetActive(true))
            .SetTarget(_scanningText);
        }

    }
}
