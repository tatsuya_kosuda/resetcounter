﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace resetcounter
{
    [ExecuteInEditMode]
    public class MeshFilterBaker : MonoBehaviour
    {

        private MeshFilter[] _meshFilters = default;

        private ComputeBuffer _positionBuffer, _uvBuffer;

        [SerializeField]
        private ComputeShader _compute = default;

        [SerializeField]
        private RenderTexture _positionMap = default, _uvMap = default;

        private RenderTexture _tmpPositionMap, _tmpUVMap;

        private List<Vector3> _positionList = new List<Vector3>();
        private List<Vector2> _uvList = new List<Vector2>();

        private void OnDisable()
        {
            if (_positionBuffer != null) { _positionBuffer.Dispose(); }

            if (_uvBuffer != null) { _uvBuffer.Dispose(); }

            if (_tmpPositionMap != null) { DestroyImmediate(_tmpPositionMap); }

            if (_tmpUVMap != null) { DestroyImmediate(_tmpUVMap); }

            _positionBuffer = null;
            _uvBuffer = null;
            _tmpPositionMap = null;
            _tmpUVMap = null;
            _meshFilters = null;
            _positionMap.Release();
            _uvMap.Release();
        }

        private void Update()
        {
            if (_meshFilters == null || _meshFilters.Length <= 0) { _meshFilters = GetComponentsInChildren<MeshFilter>(true); }

            if (_meshFilters == null || _meshFilters.Length <= 0 || _positionMap == null || _uvMap == null || _compute == null) { return; }

            _positionList.Clear();
            _uvList.Clear();
            var meshFilters = _meshFilters.Where(x => x.gameObject.activeInHierarchy).ToArray();

            foreach (var meshFilter in meshFilters)
            {
                var positions = new List<Vector3>();
                var mesh = meshFilter.sharedMesh;

                if (mesh == null) { continue; }

                //mesh.GetVertices(positions);
                //_uvList = mesh.uv.ToList();
                // increase position
                positions = GetIncreasePositions(mesh, 100);

                for (int i = 0; i < positions.Count; i++)
                {
                    positions[i] = meshFilter.transform.localToWorldMatrix.MultiplyPoint(positions[i]);
                }

                _positionList.AddRange(positions);
            }

            if (_positionList == null || _positionList.Count <= 0) { return; }

            TransferToComputeShader();
        }

        private void TransferToComputeShader()
        {
            int mapWidth = _positionMap.width;
            int mapHeight = _positionMap.height;
            int vcount = _positionList.Count;
            int vcountX3 = vcount * 3;
            int vcountX2 = vcount * 2;

            if (_positionBuffer == null) { _positionBuffer = new ComputeBuffer(vcountX3, sizeof(float)); }

            if (_uvBuffer == null) { _uvBuffer = new ComputeBuffer(vcountX2, sizeof(float)); }

            if (_positionBuffer != null && _positionBuffer.count != vcountX3)
            {
                _positionBuffer.Dispose();
                _positionBuffer = null;
            }

            if (_uvBuffer != null && _uvBuffer.count != vcountX2)
            {
                _uvBuffer.Dispose();
                _uvBuffer = null;
            }

            if (_positionBuffer == null || _uvBuffer == null) { return; }

            if (_tmpPositionMap == null)
            {
                _tmpPositionMap = new RenderTexture(mapWidth, mapHeight, 0, _positionMap.format);
                _tmpPositionMap.enableRandomWrite = true;
                _tmpPositionMap.Create();
            }

            if (_tmpUVMap == null)
            {
                _tmpUVMap = new RenderTexture(mapWidth, mapHeight, 0, _uvMap.format);
                _tmpUVMap.enableRandomWrite = true;
                _tmpUVMap.Create();
            }

            _compute.SetInt("VertexCount", vcount);
            _positionBuffer.SetData(_positionList);
            _uvBuffer.SetData(_uvList);
            _compute.SetBuffer(0, "PositionBuffer", _positionBuffer);
            _compute.SetTexture(0, "PositionMap", _tmpPositionMap);
            _compute.SetBuffer(0, "UVBuffer", _uvBuffer);
            _compute.SetTexture(0, "UVMap", _tmpUVMap);
            _compute.Dispatch(0, mapWidth / 8, mapHeight / 8, 1);
            Graphics.CopyTexture(_tmpPositionMap, _positionMap);
            Graphics.CopyTexture(_tmpUVMap, _uvMap);
        }

        private List<Vector3> GetIncreasePositions(Mesh mesh, float pcountPerArea = 1f)
        {
            var res = new List<Vector3>();
            var tris = mesh.triangles;
            var uvs = mesh.uv;
            var verts = new List<Vector3>();
            mesh.GetVertices(verts);

            for (int i = 0; i < tris.Length; i += 3)
            {
                int idx0 = tris[i];
                int idx1 = tris[i + 1];
                int idx2 = tris[i + 2];
                Vector3 pos0 = verts[idx0];
                Vector3 pos1 = verts[idx1];
                Vector3 pos2 = verts[idx2];
                Vector2 uv0 = uvs[idx0];
                Vector2 uv1 = uvs[idx1];
                Vector2 uv2 = uvs[idx2];
                var area = Vector3.Cross(pos1 - pos0, pos2 - pos0).magnitude * 0.5f;
                var pointnum = Mathf.CeilToInt(area * pcountPerArea);

                for (int pIdx = 0; pIdx < pointnum; pIdx++)
                {
                    float wait0 = Random.value;
                    float wait1 = Random.value;

                    if (wait0 + wait1 > 1)
                    {
                        wait0 = 1 - wait0;
                        wait1 = 1 - wait1;
                    }

                    float wait2 = 1 - wait0 - wait1;
                    Vector3 pos = pos0 * wait0 + pos1 * wait1 + pos2 * wait2;
                    Vector2 uv = uv0 * wait0 + uv1 * wait1 + uv2 * wait2;
                    res.Add(pos);
                    _uvList.Add(uv);
                }
            }

            return res;
        }

    }
}

