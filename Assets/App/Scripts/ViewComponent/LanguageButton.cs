﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace resetcounter
{
    public class LanguageButton : MonoBehaviour
    {

        [SerializeField]
        private Language _lang = Language.JA;

        public System.Action<Language> onClick;

        private Button _self;

        private void Awake()
        {
            _self = GetComponent<Button>();
        }

        private void OnEnable()
        {
            _self.onClick.AddListener(() => onClick?.Invoke(_lang));
        }

        private void OnDisable()
        {
            _self.onClick.RemoveAllListeners();
        }

    }
}
