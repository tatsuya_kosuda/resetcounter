﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using klib;

namespace resetcounter
{
    public class IntroView : MonoBehaviour
    {

        private MediaPlayer _mediaPlayer;

        public System.Action<AppStatus> onNext;

        private const string VIDEO_PATH = "Movies/intro.mp4";

        private void Awake()
        {
            _mediaPlayer = GetComponentInChildren<MediaPlayer>();
        }

        private void OnEnable()
        {
            _mediaPlayer.Events.AddListener(MediaPlayerEventHandler);
            _mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, VIDEO_PATH);
        }

        private void OnDisable()
        {
            _mediaPlayer.Events.RemoveListener(MediaPlayerEventHandler);
            _mediaPlayer.CloseVideo();
        }

        private void MediaPlayerEventHandler(MediaPlayer mediaPlayer, MediaPlayerEvent.EventType eventType, ErrorCode errorCode)
        {
            if (eventType == MediaPlayerEvent.EventType.FinishedPlaying)
            {
                Debug.Log("Finished Intro Movie");
                onNext?.Invoke(AppStatus.SENSING);
                gameObject.SetActive(false);
            }

            if (eventType == MediaPlayerEvent.EventType.Started)
            {
                DelayedCall.Execute(.3f, () =>
                {
                    AudioManager.Instance.PlaySE("01_Start_C");
                }, this);
                DelayedCall.Execute(5f, () =>
                {
                    AudioManager.Instance.PlaySE("04_Positive_02_C_Shorter");
                }, this);
            }
        }

    }
}
