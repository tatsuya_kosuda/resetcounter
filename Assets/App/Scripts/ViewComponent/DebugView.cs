﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using klib;

namespace resetcounter
{
    public class DebugView : MonoBehaviour
    {

        [SerializeField]
        private Button _closeButton = default;

        [SerializeField]
        private DebugUIButton _sendLogButton = default;

        [SerializeField]
        private InputField _regionCode = default, _storeCode = default;

        public System.Action sendLog;

        private void OnEnable()
        {
            _closeButton.onClick.AddListener(Close);
            _regionCode.onEndEdit.AddListener(OnEndEditRegionCode);
            _storeCode.onEndEdit.AddListener(OnEndEditStoreCode);
            _sendLogButton.onClick = SendLog;

            if (PlayerPrefs.GetString("region_code").IsNotNullOrEmpty()) { _regionCode.text = PlayerPrefs.GetString("region_code"); }

            if (PlayerPrefs.GetString("store_code").IsNotNullOrEmpty()) { _storeCode.text = PlayerPrefs.GetString("store_code"); }
        }

        private void OnDisable()
        {
            _closeButton.onClick.RemoveAllListeners();
            _regionCode.onEndEdit.RemoveAllListeners();
            _storeCode.onEndEdit.RemoveAllListeners();
        }

        private void Close()
        {
            gameObject.SetActive(false);
        }

        private void SendLog()
        {
            sendLog?.Invoke();
        }

        private void OnEndEditRegionCode(string regionCode)
        {
            PlayerPrefs.SetString("region_code", regionCode);
        }

        private void OnEndEditStoreCode(string storeCode)
        {
            PlayerPrefs.SetString("store_code", storeCode);
        }

    }
}
