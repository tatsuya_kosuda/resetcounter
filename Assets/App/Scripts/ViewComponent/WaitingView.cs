using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using klib;
using DG.Tweening;
using TMPro;
using UniRx;
using UniRx.Triggers;
using RenderHeads.Media.AVProVideo;

namespace resetcounter
{
    public class WaitingView : MonoBehaviour
    {

        [SerializeField]
        private Button _screenButton = default;

        [SerializeField]
        private CanvasGroup _selectLanguageRoot = default;

        [SerializeField]
        private LanguageButton[] _languageButtons = default;

        public System.Action<Language> onSelectedLanguage;

        public System.Action<AppStatus> onNext;

        [SerializeField]
        private DisplayUGUI _displayUGUI = default;

        [SerializeField]
        private MediaPlayer _mediaPlayer = default;

        [SerializeField]
        private Button _leftUpButton = default;

        public System.Action showDebugView;

        private const string VIDEO_PATH_TH = "Movies/20210719_ULTIMUNE_Thai_FullRes.mp4";
        private const string VIDEO_PATH_CH = "Movies/20210719_ULTIMUNE_Ch_FullRes.mp4";
        private const string VIDEO_PATH_EN = "Movies/20210719_ULTIMUNE_En_FullRes.mp4";
        private const string VIDEO_PATH_JP = "Movies/20210719_ULTIMUNE_Jp_FullRes.mp4";

        private void Awake()
        {
            Init();
        }

        private void OnEnable()
        {
            _screenButton.onClick.AddListener(OnClickScreenButton);
            _languageButtons.ForEach(x => x.onClick = OnClickLanguageButton);
            _leftUpButton
            .OnPointerDownAsObservable()
            .SelectMany(_ => Observable.Timer(System.TimeSpan.FromSeconds(2)))
            .TakeUntil(_leftUpButton.OnPointerUpAsObservable())
            .DoOnCompleted(() =>
            {
            })
            .RepeatUntilDisable(_leftUpButton)
            .Subscribe(time =>
            {
                showDebugView?.Invoke();
            });

            switch (Application.systemLanguage)
            {
                case SystemLanguage.Thai:
                    _mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, VIDEO_PATH_TH);
                    break;

                case SystemLanguage.Chinese:
                case SystemLanguage.ChineseSimplified:
                case SystemLanguage.ChineseTraditional:
                    _mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, VIDEO_PATH_CH);
                    break;

                case SystemLanguage.Japanese:
                    _mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, VIDEO_PATH_JP);
                    break;

                case SystemLanguage.English:
                default:
                    _mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, VIDEO_PATH_EN);
                    break;
            }
        }

        private void OnDisable()
        {
            _screenButton.onClick.RemoveAllListeners();
            _mediaPlayer.CloseVideo();
        }

        private void Init()
        {
            _selectLanguageRoot.gameObject.SetActive(false);
            _selectLanguageRoot.alpha = 0;
            _displayUGUI.SetAlpha(1);
        }

        public void Show()
        {
        }

        public void Hide()
        {
            if (DOTween.IsTweening(_selectLanguageRoot)) { _selectLanguageRoot.DOKill(); }

            _selectLanguageRoot.DOFade(0f, .3f).OnComplete(() =>
            {
                Init();
                gameObject.SetActive(false);
                onNext?.Invoke(AppStatus.INTRO);
            });
        }

        private void OnClickScreenButton()
        {
            _selectLanguageRoot.gameObject.SetActive(true);
            _selectLanguageRoot.DOFade(1f, .3f);
            _displayUGUI.DOFade(0f, .3f);
        }

        private void OnClickLanguageButton(Language lang)
        {
            onSelectedLanguage?.Invoke(lang);
            Hide();
        }

    }
}
