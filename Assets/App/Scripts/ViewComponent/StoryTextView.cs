using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using klib;
using UnityEngine.UI;

namespace resetcounter
{
    public class StoryTextView : MonoBehaviour
    {

        [SerializeField]
        private TextMeshProUGUI _upperCenterText = default, _upperLeftText = default, _upperTextWithNote = default, _note = default;

        //[SerializeField]
        //private Text _text = default;

        [SerializeField]
        private TMP_FontAsset[] _fonts = default;

        [SerializeField]
        private DayTexts _dayTexts = default;

        private void Awake()
        {
            _upperCenterText.SetAlpha(0);
            _upperLeftText.SetAlpha(0);
        }

        public void IntroView()
        {
            if (DOTween.IsTweening(this)) { DOTween.Kill(this); }

            var seq = DOTween.Sequence();
            seq
            .InsertCallback(.5f, () => AnimateUpperCenterText("3-1", Color.black, true, false))
            .InsertCallback(5f, () => AnimateUpperCenterText("3-2", Color.black, false, true))
            .SetTarget(this);
        }

        public void SensingView()
        {
            if (DOTween.IsTweening(this)) { DOTween.Kill(this); }

            var seq = DOTween.Sequence();
            seq
            .InsertCallback(.3f, () => AnimateUpperLeftText("4", Color.white, true, false))
            .SetTarget(this);
        }

        public void BeforeVisualizeView()
        {
            if (DOTween.IsTweening(this)) { DOTween.Kill(this); }

            var seq = DOTween.Sequence();
            seq
            .InsertCallback(0f, () => _upperLeftText.DOFade(0f, .3f))
            .InsertCallback(10f, () => AnimateUpperCenterText("6", Color.white, true, true))
            .SetTarget(this);
        }

        public void VisualizeView(DiagnoticsResult result)
        {
            if (DOTween.IsTweening(this)) { DOTween.Kill(this); }

            string prefix = "";

            switch (result)
            {
                case DiagnoticsResult.ENE:
                    prefix = "ENE";
                    break;

                case DiagnoticsResult.REF:
                    prefix = "REF";
                    break;

                case DiagnoticsResult.REL:
                    prefix = "REL";
                    break;
            }

            var seq = DOTween.Sequence();
            seq
            .InsertCallback(1f, () => AnimateUpperCenterText("7-" + prefix, Color.white, true, false))
            .InsertCallback(7f, () => AnimateUpperCenterText("8-" + prefix, Color.white, false, false))
            .InsertCallback(11.5f, () => AnimateUpperCenterText("9", Color.white, false, false))
            .InsertCallback(18.5f, () => AnimateUpperCenterText("10", Color.white, false, true))
            .InsertCallback(26f, () => AnimateUpperCenterText("11-" + prefix, Color.white, true, false))
            .InsertCallback(30f, () => AnimateUpperCenterText("12", Color.white, false, true))
            .InsertCallback(42.5f, () => AnimateUpperLeftText("14", Color.black, true, false))
            //.InsertCallback(48f, () => AudioManager.Instance.PlaySE("08_Attention_03_C_Shorter"))
            .InsertCallback(46.5f, () => AnimateUpperLeftText("15-1", Color.black, false, false))
            .InsertCallback(49.8f, () => AnimateUpperLeftText("15-2", Color.black, false, false))
            .InsertCallback(53.2f, () => AnimateUpperLeftText("15-3", Color.black, false, false))
            .InsertCallback(56.5f, () => AnimateUpperLeftText("16", Color.black, false, true))
            .InsertCallback(63f, () => AnimateUpperLeftText("17-1", Color.black, true, false))
            .InsertCallback(66.5f, () => AnimateUpperLeftText("17-2", Color.black, false, false))
            .InsertCallback(70.5f, () => AnimateUpperLeftText("17-3", Color.black, false, false))
            .InsertCallback(74f, () => AnimateUpperLeftText("17-4", Color.black, false, false))
            .InsertCallback(79f, () => AnimateUpperLeftText("18", Color.black, false, true))
            .InsertCallback(79f, () => _dayTexts.Show())
            .InsertCallback(84f, () => AnimateUpperCenterText("19", Color.black, true, false))
            .InsertCallback(88f, () => AudioManager.Instance.PlaySE("02_End_D"))
            .InsertCallback(87f, () => AnimateUpperCenterText("20", Color.black, false, true));
        }

        private void AnimateUpperCenterText(string id, Color color, bool withFadeIn, bool withFadeOut)
        {
            if (DOTween.IsTweening(_upperCenterText)) { DOTween.Complete(_upperCenterText); }

            var info = LocalizationUtils.GetText(id);
            _upperCenterText.font = _fonts[(int) LocalizationUtils.lang];
            _upperCenterText.text = info.text;
            _upperCenterText.color = color;
            var seq = DOTween.Sequence();

            if (withFadeIn)
            {
                _upperCenterText.SetAlpha(0);
                seq
                .Append(_upperCenterText.DOFade(1f, 0.3f));
            }

            if (withFadeOut)
            {
                seq
                .AppendInterval(info.duration)
                .Append(_upperCenterText.DOFade(0f, 0.3f));
            }

            seq.SetTarget(_upperCenterText);
        }

        private void AnimateUpperLeftText(string id, Color color, bool withFadeIn, bool withFadeOut)
        {
            if (DOTween.IsTweening(_upperLeftText)) { DOTween.Complete(_upperLeftText); }

            var info = LocalizationUtils.GetText(id);
            _upperLeftText.font = _fonts[(int)LocalizationUtils.lang];
            _upperLeftText.text = info.text;
            _upperLeftText.color = color;
            _upperLeftText.characterSpacing = info.characterSpacing[(int)LocalizationUtils.lang];

            if (((id == "15-1" || id == "16" || id == "17-3" || id =="17-4") && LocalizationUtils.lang == Language.EN) ||
                ((id == "15-1" || id == "15-2" || id == "16") && LocalizationUtils.lang == Language.TH))
            {
                _upperLeftText.verticalAlignment = VerticalAlignmentOptions.Top;
            }
            else
            {
                _upperLeftText.verticalAlignment = VerticalAlignmentOptions.Middle;
            }

            var seq = DOTween.Sequence();

            if (withFadeIn)
            {
                _upperLeftText.SetAlpha(0);
                seq
                .Append(_upperLeftText.DOFade(1f, 0.3f));
            }

            if (withFadeOut)
            {
                seq
                .AppendInterval(info.duration)
                .Append(_upperLeftText.DOFade(0f, 0.3f));
            }

            seq.SetTarget(_upperLeftText);
        }

        private void AnimateUpperCenterTextWithNote(string id, Color color, bool withFadeIn, bool withFadeOut)
        {
            if (DOTween.IsTweening(_upperTextWithNote)) { DOTween.Complete(_upperTextWithNote); }

            var info = LocalizationUtils.GetText(id);
            _upperTextWithNote.font = _fonts[(int)LocalizationUtils.lang];
            _upperTextWithNote.text = info.text;
            _upperTextWithNote.color = color;
            var seq = DOTween.Sequence();

            if (withFadeIn)
            {
                _upperTextWithNote.SetAlpha(0);
                seq
                .Append(_upperTextWithNote.DOFade(1f, 0.3f));
            }

            if (withFadeOut)
            {
                seq
                .AppendInterval(info.duration)
                .Append(_upperTextWithNote.DOFade(0f, 0.3f));
            }

            seq.SetTarget(_upperTextWithNote);
        }

        private void AnimateNoteText(string id, Color color, bool withFadeIn, bool withFadeOut)
        {
            if (DOTween.IsTweening(_note)) { DOTween.Complete(_note); }

            var info = LocalizationUtils.GetText(id);
            _note.font = _fonts[(int)LocalizationUtils.lang];
            _note.text = info.text;
            _note.color = color;
            var seq = DOTween.Sequence();

            if (withFadeIn)
            {
                _note.SetAlpha(0);
                seq
                .Append(_note.DOFade(1f, 0.3f));
            }

            if (withFadeOut)
            {
                seq
                .AppendInterval(info.duration)
                .Append(_note.DOFade(0f, 0.3f));
            }

            seq.SetTarget(_note);
        }

    }
}
