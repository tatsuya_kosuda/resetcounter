﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace resetcounter
{
    public class LayoutLangButtons : MonoBehaviour
    {

        [SerializeField]
        private LanguageButton _zhcn = default, _zhcw = default, _en = default, _th = default, _jp = default;

        private void OnEnable()
        {
            switch (Application.systemLanguage)
            {
                case SystemLanguage.Thai:
                    ThaiLayout();
                    break;

                case SystemLanguage.Chinese:
                case SystemLanguage.ChineseSimplified:
                case SystemLanguage.ChineseTraditional:
                default:
                    ChineseLayout();
                    break;
            }
        }

        private void ChineseLayout()
        {
            _zhcn.transform.SetSiblingIndex(0);
            _zhcw.transform.SetSiblingIndex(1);
            _en.transform.SetSiblingIndex(2);
            _jp.transform.SetSiblingIndex(3);
            _th.transform.SetSiblingIndex(4);
        }

        private void ThaiLayout()
        {
            _th.transform.SetSiblingIndex(0);
            _zhcn.transform.SetSiblingIndex(1);
            _zhcw.transform.SetSiblingIndex(2);
            _en.transform.SetSiblingIndex(3);
            _jp.transform.SetSiblingIndex(4);
        }

    }
}
