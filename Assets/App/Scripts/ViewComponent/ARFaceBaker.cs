﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace resetcounter
{
    [RequireComponent(typeof(ARFace))]
    public class ARFaceBaker : MonoBehaviour
    {

        private ARFace _arFace;

        [SerializeField]
        private ComputeShader _cs = null;

        private ComputeBuffer _pBuffer, _uvBuffer;

        private List<Vector3> _pList;
        private List<Vector2> _uvList;

        [SerializeField]
        private RenderTexture _pMap = null, _uvMap = null;

        private RenderTexture _tmpPMap, _tmpUVMap;

        [SerializeField]
        private Camera _arCamera = default;

        private void Awake()
        {
            _arFace = GetComponent<ARFace>();
        }

        private void OnEnable()
        {
            _arFace.updated += OnARFaceUpdated;
        }

        private void OnDisable()
        {
            _arFace.updated -= OnARFaceUpdated;

            if (_pBuffer != null) { _pBuffer.Dispose(); }

            if (_uvBuffer != null) { _uvBuffer.Dispose(); }

            if (_tmpPMap != null) { DestroyImmediate(_tmpPMap); }

            if (_tmpUVMap != null) { DestroyImmediate(_tmpUVMap); }

            _pBuffer = null;
            _uvBuffer = null;
            _tmpPMap = null;
            _tmpUVMap = null;
        }

        private void OnARFaceUpdated(ARFaceUpdatedEventArgs e)
        {
            if (e.face.trackingState != TrackingState.Tracking ||
                e.face.indices.IsCreated == false ||
                e.face.vertices.IsCreated == false) { return; }

            // [PREPARE] bake vertices
            // _pList = e.face.vertices.ToList();
            var mesh = new Mesh();
            mesh.SetVertices(e.face.vertices);
            mesh.SetIndices(e.face.indices, MeshTopology.Triangles, 0, false);
            _pList = GetIncreasePositions(mesh, 10);

            if (_pList == null || _pList.Count <= 0) { return; }

            for (int i = 0; i < _pList.Count; i++)
            {
                _pList[i] = transform.localToWorldMatrix.MultiplyPoint(_pList[i]);
            }

            // [PREPARE] bake colors
            if (_uvList == null) { _uvList = new List<Vector2>(); }

            _uvList.Clear();

            for (int i = 0; i < _pList.Count; i++)
            {
                var vp = _arCamera.WorldToViewportPoint(_pList[i]);
                _uvList.Add(new Vector2(vp.x, vp.y));
            }

            TransferToComputeShader();
        }

        private void TransferToComputeShader()
        {
            int mw = _pMap.width, mh = _pMap.height;
            int vcount = _pList.Count;
            int vcountX3 = vcount * 3;
            int vcountX2 = vcount * 2;

            if (_pBuffer == null) { _pBuffer = new ComputeBuffer(vcountX3, sizeof(float)); }

            if (_uvBuffer == null) { _uvBuffer = new ComputeBuffer(vcountX2, sizeof(float)); }

            if (_pBuffer != null && _pBuffer.count != vcountX3)
            {
                _pBuffer.Dispose();
                _pBuffer = null;
            }

            if (_uvBuffer != null && _uvBuffer.count != vcountX2)
            {
                _uvBuffer.Dispose();
                _uvBuffer = null;
            }

            if (_pBuffer == null || _uvBuffer == null) { return; }

            if (_tmpPMap == null)
            {
                _tmpPMap = new RenderTexture(mw, mh, 0, _pMap.format);
                _tmpPMap.enableRandomWrite = true;
                _tmpPMap.Create();
            }

            if (_tmpUVMap == null)
            {
                _tmpUVMap = new RenderTexture(mw, mh, 0, _uvMap.format);
                _tmpUVMap.enableRandomWrite = true;
                _tmpUVMap.Create();
            }

            _cs.SetInt("VertexCount", vcount);
            _pBuffer.SetData(_pList);
            _uvBuffer.SetData(_uvList);
            _cs.SetBuffer(0, "PositionBuffer", _pBuffer);
            _cs.SetTexture(0, "PositionMap", _tmpPMap);
            _cs.SetBuffer(0, "UVBuffer", _uvBuffer);
            _cs.SetTexture(0, "UVMap", _tmpUVMap);
            _cs.Dispatch(0, mw / 8, mh / 8, 1);
            Graphics.CopyTexture(_tmpPMap, _pMap);
            Graphics.CopyTexture(_tmpUVMap, _uvMap);
        }

        private List<Vector3> GetIncreasePositions(Mesh mesh, float pcountPerArea = 1f)
        {
            var res = new List<Vector3>();
            var tris = mesh.triangles;
            var verts = new List<Vector3>();
            mesh.GetVertices(verts);

            for (int i = 0; i < tris.Length; i += 3)
            {
                int idx0 = tris[i];
                int idx1 = tris[i + 1];
                int idx2 = tris[i + 2];
                Vector3 pos0 = verts[idx0];
                Vector3 pos1 = verts[idx1];
                Vector3 pos2 = verts[idx2];
                var area = Vector3.Cross(pos1 - pos0, pos2 - pos0).magnitude * 0.5f;
                var pointnum = Mathf.CeilToInt(area * pcountPerArea);

                for (int pIdx = 0; pIdx < pointnum; pIdx++)
                {
                    //float wait0 = Random.value;
                    float wait0 = Mathf.PerlinNoise((idx0 + pIdx) * 100, (idx1 + pIdx) * 100);
                    //float wait1 = Random.value;
                    float wait1 = Mathf.PerlinNoise((idx1 + pIdx) * 100, (idx2 + pIdx) * 100);

                    if (wait0 + wait1 > 1)
                    {
                        wait0 = 1 - wait0;
                        wait1 = 1 - wait1;
                    }

                    float wait2 = 1 - wait0 - wait1;
                    Vector3 pos = pos0 * wait0 + pos1 * wait1 + pos2 * wait2;
                    res.Add(pos);
                }
            }

            return res;
        }

    }
}
