using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using klib;
using UniRx;
using UnityEngine.UI;
using DG.Tweening;

namespace resetcounter
{
    public class VisualizeView : MonoBehaviour
    {

        [SerializeField]
        private MediaPlayer _mediaPlayer = default;

        [SerializeField]
        private Image _gauge = default;

        public System.Action onEndVideo;

        private const string VIDEO_PATH = "Movies/visualize";

        private const float DIFFUSE_START_TIME = 14f;

        private const float DIFFUSE_INTERVAL = 15f;

        private const int DIFFUSE_TIME = 5;

        private const float DIFFUSE_END_TIME = 30f;

        private void OnEnable()
        {
            _mediaPlayer.Events.AddListener(MediaPlayerEventHandler);
        }

        private void OnDisable()
        {
            _mediaPlayer.Events.RemoveListener(MediaPlayerEventHandler);
            _mediaPlayer.CloseVideo();
            _mediaPlayer.SetActive(false);
            _gauge.DOComplete();
            _gauge.rectTransform.localScale = new Vector3(0, 1, 1);
        }

        public void StartVideo(DiagnoticsResult result)
        {
            Debug.Log($"[Visualize View] diagnotics result = {result}");
            _mediaPlayer.SetActive(true);
            string video = "";

            switch (result)
            {
                case DiagnoticsResult.ENE:
                    video = VIDEO_PATH + "_ENE.mp4";
                    break;

                case DiagnoticsResult.REF:
                    video = VIDEO_PATH + "_REF.mp4";
                    break;

                case DiagnoticsResult.REL:
                    video = VIDEO_PATH + "_REL.mp4";
                    break;
            }

            _mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, video);
        }

        private void MediaPlayerEventHandler(MediaPlayer mediaPlayer, MediaPlayerEvent.EventType eventType, ErrorCode errorCode)
        {
            if (eventType == MediaPlayerEvent.EventType.FinishedPlaying)
            {
                Debug.Log("Finished VISUALIZE - OUTRO Movie");
                onEndVideo?.Invoke();
            }
        }

        private void Diffuse()
        {
            DelayedCall.Execute(DIFFUSE_START_TIME, () =>
            {
                UnityAromaShooter.Diffuse(DIFFUSE_TIME * 1000);
                Observable
                .Interval(System.TimeSpan.FromSeconds(DIFFUSE_INTERVAL))
                .TakeWhile(x => x + 1 < (DIFFUSE_END_TIME - DIFFUSE_START_TIME) / DIFFUSE_INTERVAL)
                .Subscribe(x => UnityAromaShooter.Diffuse(DIFFUSE_TIME * 1000));
            }, this);
        }

        public void StartGaugeAnimation()
        {
            _gauge.rectTransform.localScale = new Vector3(0, 1, 1);
            _gauge.rectTransform.DOScaleX(1, 90).SetEase(Ease.Linear);
        }

    }
}
