﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using DG.Tweening;
using UnityEngine.XR.ARFoundation;

using klib;

namespace resetcounter
{
    public class XRView : MonoBehaviour
    {

        [SerializeField]
        private Camera _arCamera = default, _vfxCamera = default;

        public RenderTexture ARCameraRT
        {
            get;
            private set;
        }

        public RenderTexture VFXCameraRT
        {
            get;
            private set;
        }

        private const float V_FOV = 60f;

        [SerializeField]
        private Transform _planeMesh = default;

        [SerializeField]
        private VisualEffect _meshVFX = default, _arFaceVFX = default;

        [SerializeField]
        private ARFaceBaker _arFaceBaker = default;

        [SerializeField]
        private ARSession _arSession = default;

        [SerializeField]
        private MeshFilterBaker _meshFilterBaker = default;

        private static readonly int CAMERA_TEXTURE_ID = Shader.PropertyToID("CameraTexture");
#if UNITY_EDITOR
        private static readonly int POSITION_MAP_ID = Shader.PropertyToID("PositionMap");
#endif
        private static readonly int NOISE_ID = Shader.PropertyToID("Noise");
        private static readonly int ALPHA_ID = Shader.PropertyToID("Alpha");
        private static readonly int COLOR_ANIMATION_ID = Shader.PropertyToID("ColorAnimation");
        private static readonly int FINAL_COLOR_ID = Shader.PropertyToID("FinalColor");

        [SerializeField, ColorUsage(false, true)]
        private Color[] _colors = default;

        private void Awake()
        {
            // setup rendertexture
            ARCameraRT = new RenderTexture(720, 1280, 0, _arCamera.targetTexture.format);
            _arCamera.targetTexture = ARCameraRT;
            VFXCameraRT = new RenderTexture(Screen.width, Screen.height, 0, _vfxCamera.targetTexture.format);
            _vfxCamera.targetTexture = VFXCameraRT;
            // setup mesh
            var scale = _planeMesh.localScale;
            scale.z = _planeMesh.localPosition.z * Mathf.Tan(V_FOV / 2 * Mathf.Deg2Rad) * 2 / 10;
            scale.x = _planeMesh.localPosition.z * Mathf.Tan(GetHorizontalFOV() / 2 * Mathf.Deg2Rad) * 2 / 10;
            _planeMesh.localScale = scale;
            // setup vfx
#if UNITY_EDITOR
            _meshVFX.ResetOverride(CAMERA_TEXTURE_ID);
            _arFaceVFX.ResetOverride(CAMERA_TEXTURE_ID);
            _arFaceVFX.ResetOverride(POSITION_MAP_ID);
#else
            _meshVFX.SetTexture(CAMERA_TEXTURE_ID, ARCameraRT);
            _arFaceVFX.SetTexture(CAMERA_TEXTURE_ID, ARCameraRT);
#endif
            // set active false
            StopARSession();
            InitVFXParams();
        }

        public void StartARSession()
        {
            Debug.Log("START AR SESSION");
            _arSession.enabled = true;
            _arSession.Reset();
            _meshFilterBaker.SetActive(true);
            _arFaceBaker.SetActive(true);
            _meshVFX.gameObject.SetActive(true);
            _arFaceVFX.gameObject.SetActive(true);
#if UNITY_EDITOR
            _meshVFX.ResetOverride(CAMERA_TEXTURE_ID);
            _arFaceVFX.ResetOverride(CAMERA_TEXTURE_ID);
            _arFaceVFX.ResetOverride(POSITION_MAP_ID);
#else
            _meshVFX.SetTexture(CAMERA_TEXTURE_ID, ARCameraRT);
            _arFaceVFX.SetTexture(CAMERA_TEXTURE_ID, ARCameraRT);
#endif
        }

        public void StopARSession()
        {
            Debug.Log("STOP AR SESSION");
            _arSession.enabled = false;
            _meshFilterBaker.SetActive(false);
            _arFaceBaker.SetActive(false);
        }

        public void InitVFXParams()
        {
            _meshVFX.SetFloat(NOISE_ID, 0);
            _arFaceVFX.SetFloat(ALPHA_ID, 0);
            _arFaceVFX.SetFloat(NOISE_ID, 0.05f);
            _arFaceVFX.SetFloat(COLOR_ANIMATION_ID, 0);
            _meshVFX.gameObject.SetActive(false);
            _arFaceVFX.gameObject.SetActive(false);
        }

        private float GetHorizontalFOV()
        {
            float vFOVInRads = V_FOV * Mathf.Deg2Rad;
            float hFOVInRads = 2 * Mathf.Atan(Mathf.Tan(vFOVInRads / 2) * _vfxCamera.aspect);
            return hFOVInRads * Mathf.Rad2Deg;
        }

        public Sequence BeforeVisualize(DiagnoticsResult result)
        {
            Debug.Log($"[XR View] diagnotics result = {result}");
            _arFaceVFX.SetVector4(FINAL_COLOR_ID, _colors[(int)result - 1]);
            var seq = DOTween.Sequence();
            seq
            .InsertCallback(8f, () => _meshVFX.SetFloat(NOISE_ID, 1))
            .Insert(8f, DOTween.To(() => _arFaceVFX.GetFloat(ALPHA_ID), a =>
            {
                _arFaceVFX.SetFloat(ALPHA_ID, a);
            }, 1, 2f))
            .Insert(9f, DOTween.To(() => _arFaceVFX.GetFloat(NOISE_ID), n =>
            {
                _arFaceVFX.SetFloat(NOISE_ID, n);
            }, 0, 2).SetEase(Ease.InOutQuad))
            .Insert(12f, DOTween.To(() => _arFaceVFX.GetFloat(NOISE_ID), n =>
            {
                _arFaceVFX.SetFloat(NOISE_ID, n);
            }, 0.1f, 2).SetEase(Ease.OutCirc))
            .Insert(13f, DOTween.To(() => _arFaceVFX.GetFloat(COLOR_ANIMATION_ID), c =>
            {
                _arFaceVFX.SetFloat(COLOR_ANIMATION_ID, c);
            }, 1, 1))
            .Insert(15f, DOTween.To(() => _arFaceVFX.GetFloat(ALPHA_ID), a =>
            {
                _arFaceVFX.SetFloat(ALPHA_ID, a);
            }, 0f, 1f).SetEase(Ease.InQuad))
            .InsertCallback(16f, () =>
            {
                _meshVFX.gameObject.SetActive(false);
                _arFaceVFX.gameObject.SetActive(false);
            });
            return seq;
        }

    }
}
