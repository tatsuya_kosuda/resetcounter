using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

namespace resetcounter
{
    public class DayTexts : MonoBehaviour
    {

        [SerializeField]
        private TextMeshProUGUI _day1Text = default, _day2Text = default, _day3Text = default, _day1TitleText = default, _day2TitleText = default, _day3TitleText = default;

        [SerializeField]
        private TMP_FontAsset[] _fonts = default;

        [SerializeField]
        private CanvasGroup _day1Canvas = default, _day2Canvas = default, _day3Canvas = default;

        private const string ID1 = "18-DAY1", ID2 = "18-DAY2", ID3 = "18-DAY3";

        private static readonly Vector2[] DAY1POS = new Vector2[5]
        {
            new Vector2(0, -292),
            new Vector2(0, -292),
            new Vector2(44, -292),
            new Vector2(-2, -292),
            new Vector2(37, -289)
        };
        private static readonly Vector2[] DAY2POS = new Vector2[5]
        {
            new Vector2(0, -292),
            new Vector2(-2, -292),
            new Vector2(27, -292),
            new Vector2(27, -292),
            new Vector2(0, -288)
        };
        private static readonly Vector2[] DAY3POS = new Vector2[5]
        {
            new Vector2(8, -361),
            new Vector2(7, -361),
            new Vector2(7, -361),
            new Vector2(7, -361),
            new Vector2(8, -361)
        };

        private static readonly Vector2[] DAY1SIZE = new Vector2[5]
        {
            new Vector2(542, 344),
            new Vector2(542, 344),
            new Vector2(630, 344),
            new Vector2(622, 344),
            new Vector2(586, 348)
        };
        private static readonly Vector2[] DAY2SIZE = new Vector2[5]
        {
            new Vector2(547, 344),
            new Vector2(592, 344),
            new Vector2(633, 344),
            new Vector2(633, 344),
            new Vector2(586, 350)
        };
        private static readonly Vector2[] DAY3SIZE = new Vector2[5]
        {
            new Vector2(531, 480),
            new Vector2(617, 480),
            new Vector2(617, 480),
            new Vector2(617, 480),
            new Vector2(620, 480)
        };

        public void Show()
        {
            if (DOTween.IsTweening(this)) { DOTween.Complete(this); }

            {
                var info = LocalizationUtils.GetText(ID1);
                _day1Text.font = _fonts[(int)LocalizationUtils.lang];
                _day1Text.text = info.text;
                _day1Text.characterSpacing = info.characterSpacing[(int)LocalizationUtils.lang];
                _day1Text.rectTransform.anchoredPosition = DAY1POS[(int)LocalizationUtils.lang];
                _day1Text.rectTransform.sizeDelta = DAY1SIZE[(int)LocalizationUtils.lang];
            }

            {
                var info = LocalizationUtils.GetText(ID2);
                _day2Text.font = _fonts[(int)LocalizationUtils.lang];
                _day2Text.text = info.text;
                _day2Text.characterSpacing = info.characterSpacing[(int)LocalizationUtils.lang];
                _day2Text.rectTransform.anchoredPosition = DAY2POS[(int)LocalizationUtils.lang];
                _day2Text.rectTransform.sizeDelta = DAY2SIZE[(int)LocalizationUtils.lang];
            }

            {
                var info = LocalizationUtils.GetText(ID3);
                _day3Text.font = _fonts[(int)LocalizationUtils.lang];
                _day3Text.text = info.text;
                _day3Text.characterSpacing = info.characterSpacing[(int)LocalizationUtils.lang];
                _day3Text.rectTransform.anchoredPosition = DAY3POS[(int)LocalizationUtils.lang];
                _day3Text.rectTransform.sizeDelta = DAY3SIZE[(int)LocalizationUtils.lang];
            }

            if (LocalizationUtils.lang == Language.ZHCN || LocalizationUtils.lang == Language.ZHTW)
            {
                _day1TitleText.font = _fonts[(int)LocalizationUtils.lang];
                _day1TitleText.text = "第一天";
                _day2TitleText.font = _fonts[(int)LocalizationUtils.lang];
                _day2TitleText.text = "第二天";
                _day3TitleText.font = _fonts[(int)LocalizationUtils.lang];
                _day3TitleText.text = "第三天";
            }
            else
            {
                _day1TitleText.font = _fonts[(int)Language.EN];
                _day1TitleText.text = "Day 1";
                _day2TitleText.font = _fonts[(int)Language.EN];
                _day2TitleText.text = "Day 2";
                _day3TitleText.font = _fonts[(int)Language.EN];
                _day3TitleText.text = "Day 3";
            }

            var seq = DOTween.Sequence();
            seq
            .Insert(0f, _day1Canvas.DOFade(1f, 0.7f))
            .Insert(0.7f, _day2Canvas.DOFade(1f, 0.7f))
            .Insert(1.4f, _day3Canvas.DOFade(1f, 0.7f))
            .Insert(3.4f, _day1Canvas.DOFade(0f, 1f))
            .Insert(3.4f, _day2Canvas.DOFade(0f, 1f))
            .Insert(3.4f, _day3Canvas.DOFade(0f, 1f))
            .OnComplete(() =>
            {
                _day1Canvas.alpha = 0;
                _day2Canvas.alpha = 0;
                _day3Canvas.alpha = 0;
            })
            .SetTarget(this);
        }

    }
}
