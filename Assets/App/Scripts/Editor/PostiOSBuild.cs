﻿using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEngine;

public class PostiOSBuild
{

    [PostProcessBuild]
    public static void OnPostProcessBuild(BuildTarget target, string pathToBbuildTarget)
    {
        if (target != BuildTarget.iOS) { return; }

        SetXcodePlist(pathToBbuildTarget);
    }

    private static void SetXcodePlist(string pathToBuildTarget)
    {
        var plistPath = pathToBuildTarget + "/Info.plist";
        var plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(plistPath));
        var rootDict = plist.root;
        rootDict.SetBoolean("UIFileSharingEnabled", true);
        File.WriteAllText(plistPath, plist.WriteToString());
    }

}