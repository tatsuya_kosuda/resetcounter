﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using klib;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace resetcounter
{
    public static class LogUtils
    {

        public static void SaveDataToJson<T>(T obj, string savePath)
        {
            string dirPath = Path.GetDirectoryName(savePath);

            if (dirPath.IsNotNullOrEmpty() && !Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            string json = JsonConvert.SerializeObject(obj, Formatting.Indented, new IsoDateTimeConverter() { DateTimeFormat = "yyyy/MM/dd HH:mm:ss" });
            FileStream fs = File.Create(savePath);
            var utf8_encoding = new System.Text.UTF8Encoding(false);
            StreamWriter sw = new StreamWriter(fs, utf8_encoding);
            sw.WriteLine(json);
            sw.Close();
            fs.Close();
        }

        public static T LoadDataFromJson<T>(string loadPath, FileAccess fileAccess = FileAccess.ReadWrite) where T : new ()
        {
            if (!File.Exists(loadPath))
            {
                return new T();
            }

            FileStream fs = File.Open(loadPath, FileMode.Open, fileAccess);
            StreamReader sr = new StreamReader(fs);
            var json = JsonConvert.DeserializeObject<T>(sr.ReadToEnd(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy/MM/dd HH:mm:ss" });
            sr.Close();
            fs.Close();
            return json;
        }

        public static string LoadBeforeData(string loadPath, FileAccess fileAccess = FileAccess.ReadWrite)
        {
            if (!File.Exists(loadPath))
            {
                return null;
            }

            FileStream fs = File.Open(loadPath, FileMode.Open, fileAccess);
            StreamReader sr = new StreamReader(fs);
            string str = sr.ReadToEnd();
            return str;
        }

    }
}
