﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using klib;
using System.Linq;

namespace resetcounter
{
    public static class LocalizationUtils
    {

        private static List<LocalizeText> _texts = new List<LocalizeText>();

        public static Language lang = Language.JA;

        private const string FILE_NAME = "Localization";

        private static LocalizeTextSettings _setting;

        private static void ReadCSV()
        {
            var csvFile = Resources.Load(FILE_NAME) as TextAsset;

            using (var reader = new StringReader(csvFile.text))
            {
                // skip 3 line
                reader.ReadLine();
                reader.ReadLine();
                reader.ReadLine();

                while (reader.Peek() > -1)
                {
                    var lineData = reader.ReadLine();
                    var data = new LocalizeText(lineData.Split("\t".ToCharArray()));
                    _texts.Add(data);
                }
            }
        }

        public static LocalizeTextInfo GetText(string id)
        {
            if (_texts.IsNullOrEmpty()) { ReadCSV(); }

#if UNITY_EDITOR

            if (_setting == null) { _setting = DataUtils.LoadDataFromJson<LocalizeTextSettings>(LocalizeTextSettings.PATH); }

#else

            if (_setting == null) { _setting = DataUtils.LoadDataFromJson<LocalizeTextSettings>(LocalizeTextSettings.PATH, FileAccess.Read); }

#endif
            string text = _texts.FirstOrDefault(x => x.id == id).GetText(lang);
            float duration = _setting.settings.FirstOrDefault(x => x.id == id).duration;
            float[] characterSpacing = _setting.settings.FirstOrDefault(x => x.id == id).characterSpacing;
            return new LocalizeTextInfo() { text = text, duration = duration, characterSpacing = characterSpacing };
        }

    }
}
