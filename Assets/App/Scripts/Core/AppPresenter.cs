using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.XR.ARFoundation;
using klib;
using System.Linq;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace resetcounter
{
    public class AppPresenter : MonoBehaviour
    {

        private AppModel _model;
        private AppView _view;

        private List<System.IDisposable> _streams = new List<System.IDisposable>();

        private System.IDisposable _timer;

        [SerializeField]
        private Camera _arCamera = default;

        private ARFaceManager _arFaceManager;

        private List<LogData> _logData = new List<LogData>();

        private long _beforeStatusCode;

        private void Awake()
        {
            _model = new AppModel();
            _view = GetComponentInChildren<AppView>();
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            _arFaceManager = GetComponentInChildren<ARFaceManager>();
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 30;
            var beforeLogData = LogUtils.LoadDataFromJson<RequestData>(RequestData.PATH);

            if (beforeLogData.log_data.IsNotNullOrEmpty()) { _logData = beforeLogData.log_data.ToList(); }
        }

        private void OnEnable()
        {
            _streams.Add(_model.AppStatus.Subscribe(OnChangeAppStatus));
            _streams.Add(_model.IsDetectedFace.Subscribe(OnChangeIsDetectedFace));
            _view.onSelectedLanguage = lang =>
            {
                LocalizationUtils.lang = lang;
                // log
                var data = new LogData(ActionType.CHANGE_LANG, _model.TransactionId, 0, lang);
                _logData.Add(data);
            };
            _view.onNext = status => _model.SetAppStatus(status);
            _view.sendLog = () =>
            {
                StartCoroutine(AutoUploadLogEnumerator());
            };
            StartCoroutine(AutoUploadLogEnumerator());
        }

        private void OnDisable()
        {
            foreach (var stream in _streams)
            {
                stream.Dispose();
            }

            _streams.Clear();
        }

        private void Start()
        {
            _model.SetAppStatus(AppStatus.WAITING);
        }

        private IEnumerator CleanEnumerator()
        {
            yield return null;
            Resources.UnloadUnusedAssets();
            yield return null;
            System.GC.Collect();
        }

        private void OnChangeAppStatus(AppStatus status)
        {
            Debug.Log($"[OnChangeAppStatus] AppStatus = {status}");

            switch (status)
            {
                case AppStatus.WAITING:
                    _model.Init();
                    _view.WaitingView();
                    StartCoroutine(CleanEnumerator());
                    break;

                case AppStatus.INTRO:
                    _view.IntroView();
                    {
                        // log
                        var data = new LogData(ActionType.DEFAULT, _model.TransactionId, 1, LocalizationUtils.lang);
                        _logData.Add(data);
                    }
                    break;

                case AppStatus.SENSING:
                    OnSensingStarted();
                    {
                        // log
                        var data = new LogData(ActionType.DEFAULT, _model.TransactionId, 2, LocalizationUtils.lang);
                        _logData.Add(data);
                    }
                    break;

                case AppStatus.VISUALIZE:
                    _timer.Dispose();
                    _arFaceManager.facesChanged -= OnARFaceChanged;
                    _view.VisualizeView(_model.GetDiagnoticsResult());
                    {
                        // log
                        var data = new LogData(ActionType.DEFAULT, _model.TransactionId, 4, LocalizationUtils.lang);
                        _logData.Add(data);
                    }
                    StartLogCollection();
                    break;
            }
        }

        private void OnSensingStarted()
        {
            _view.SensingView();
            DelayedCall.Execute(3f, () =>
            {
                _timer = Observable.Timer(System.TimeSpan.FromSeconds(10f)).Subscribe(_ => _model.SetAppStatus(AppStatus.VISUALIZE));
                _arFaceManager.facesChanged += OnARFaceChanged;
            }, this);
        }

        private void OnARFaceChanged(ARFacesChangedEventArgs e)
        {
            var face = e.updated.FirstOrDefault();

            if (face == null) { return; }

            if (face.trackingState != UnityEngine.XR.ARSubsystems.TrackingState.Tracking) { return; }

            var right = face.rightEye;
            var left = face.leftEye;
            var rightW = face.transform.localToWorldMatrix.MultiplyPoint(right.position);
            var leftW = face.transform.localToWorldMatrix.MultiplyPoint(left.position);
            var rightVP = _arCamera.WorldToViewportPoint(rightW);
            var leftVP = _arCamera.WorldToViewportPoint(leftW);

            //Debug.Log($"right = {rightVP} left = {leftVP}");

            if (rightVP.y > 0.5f && leftVP.y > 0.5f)
            {
                _model.SetIsDetectedFace(true);
            }
        }

        private void OnChangeIsDetectedFace(bool detected)
        {
            if (detected == false) { return; }

            _model.SetAppStatus(AppStatus.VISUALIZE);
            // log
            var data = new LogData(ActionType.DETECT_FACE, _model.TransactionId, 3, LocalizationUtils.lang);
            _logData.Add(data);
        }

        private void StartLogCollection()
        {
            DelayedCall.Execute(14f, () =>
            {
                // log
                var data = new LogData(ActionType.DEFAULT, _model.TransactionId, 5, LocalizationUtils.lang, _model.Result);
                _logData.Add(data);
            }, this);
            DelayedCall.Execute(64f, () =>
            {
                // log
                var data = new LogData(ActionType.TESTING, _model.TransactionId, 6, LocalizationUtils.lang, _model.Result);
                _logData.Add(data);
            }, this);
            DelayedCall.Execute(72f, () =>
            {
                // log
                var data = new LogData(ActionType.DEFAULT, _model.TransactionId, 7, LocalizationUtils.lang, _model.Result);
                _logData.Add(data);
            }, this);
            DelayedCall.Execute(90f, () =>
            {
                // log
                var data = new LogData(ActionType.DEFAULT, _model.TransactionId, 8, LocalizationUtils.lang, _model.Result);
                _logData.Add(data);
            }, this);
            DelayedCall.Execute(96f, () =>
            {
                // log
                var data = new LogData(ActionType.END, _model.TransactionId, 9, LocalizationUtils.lang, _model.Result);
                _logData.Add(data);
                var test = new RequestData();
                test.log_data = _logData.ToArray();
                LogUtils.SaveDataToJson(test, RequestData.PATH);
            }, this);
        }

        public IEnumerator AutoUploadLogEnumerator()
        {
            if (System.DateTime.TryParse(PlayerPrefs.GetString("SENDED-DATE-TIME"), out System.DateTime alreadySendedDate))
            {
                if (Application.internetReachability == NetworkReachability.NotReachable)
                {
                    Debug.LogError("NETWORK ERROR");
                    yield break;
                }

                System.DateTime now = System.DateTime.Now;

                while (alreadySendedDate.Date < now.Date)
                {
                    yield return UploadLogEnumerator(alreadySendedDate);

                    if (_beforeStatusCode != 200 && _beforeStatusCode != -1) { yield break; }

                    alreadySendedDate = System.DateTime.Parse(PlayerPrefs.GetString("SENDED-DATE-TIME"));
                }

                PlayerPrefs.SetString("SENDED-DATE-TIME", alreadySendedDate.ToString());
            }
            else
            {
                PlayerPrefs.SetString("SENDED-DATE-TIME", System.DateTime.Now.ToString());
            }

            CheckOldLogfile();
        }

        private IEnumerator UploadLogEnumerator(System.DateTime dateTime)
        {
#if UNITY_EDITOR
            string path = "Log/" + dateTime.ToString("yyyyMMdd") + ".json";
#else
            string path = Application.persistentDataPath + "/Log/" + dateTime.ToString("yyyyMMdd") + ".json";
#endif
            string uploadtxt = LogUtils.LoadBeforeData(path);
            ResponseData res = null;

            if (uploadtxt != null)
            {
#if dev
                var request = new UnityWebRequest("https://gw.digiskintester.com/dev/analytics/upload", "POST");
                request.SetRequestHeader("X-API-KEY", "ObC7P96wkF8qTEpTAVlcB9fPxtSkxhTY78Xl068u");
#elif prd
                var request = new UnityWebRequest("https://gw.digiskintester.com/prd/analytics/upload", "POST");
                request.SetRequestHeader("X-API-KEY", "OJNOeqPo3j9AzG0vb0tet7zrUdovgOBn3ShHqDsj");
#endif
                byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(uploadtxt);
                request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
                request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
                request.SetRequestHeader("Content-Type", "application/json");
                yield return request.SendWebRequest();
                res = JsonConvert.DeserializeObject<ResponseData>(request.downloadHandler.text, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });

                if (res.status != 200) { Debug.LogError($"API ERROR : status = {res.status} message = {res.message}"); }
            }

            if (res == null || res.status == 200)
            {
                if (dateTime.AddDays(1) < System.DateTime.Now)
                {
                    PlayerPrefs.SetString("SENDED-DATE-TIME", dateTime.AddDays(1).ToString());
                }
                else
                {
                    PlayerPrefs.SetString("SENDED-DATE-TIME", System.DateTime.Now.ToString());
                }
            }

            if (res == null) { _beforeStatusCode = -1; }
            else { _beforeStatusCode = res.status; }
        }

        static void CheckOldLogfile()
        {
            uint logLifespan = 30;
#if UNITY_EDITOR
            string path = "Log";
#else
            string path = Application.persistentDataPath + "/Log";
#endif

            if (path.IsNotNullOrEmpty() && !System.IO.Directory.Exists(path))
            {
                return;
            }

            System.IO.DirectoryInfo dyInfo = new System.IO.DirectoryInfo(path);
            // フォルダのファイルを取得
            var target = System.DateTime.Today.AddDays(-logLifespan);

            foreach (System.IO.FileInfo fInfo in dyInfo.GetFiles())
            {
                // 日付の比較
                if (fInfo.LastWriteTime < target)
                {
                    fInfo.Delete();
                }
            }
        }

    }
}
