using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using klib;
using DG.Tweening;

namespace resetcounter
{
    public class AppView : MonoBehaviour
    {

        [SerializeField]
        private WaitingView _waitingView = default;

        [SerializeField]
        private IntroView _introView = default;

        [SerializeField]
        private SensingView _sensingView = default;

        [SerializeField]
        private VisualizeView _visualizeView = default;

        [SerializeField]
        private XRView _xrView = default;

        [SerializeField]
        private StoryTextView _storyTextView = default;

        [SerializeField]
        private DebugView _debugView = default;

        public System.Action<Language> onSelectedLanguage;

        public System.Action<AppStatus> onNext;

        public System.Action sendLog;

        public void WaitingView()
        {
            _waitingView.SetActive(true);
            _waitingView.onSelectedLanguage = onSelectedLanguage;
            _waitingView.onNext = onNext;
            _waitingView.showDebugView = ShowDebugView;
            _waitingView.Show();
        }

        private void ShowDebugView()
        {
            _debugView.sendLog = sendLog;
            _debugView.SetActive(true);
        }

        public void IntroView()
        {
            _introView.onNext = onNext;
            _introView.SetActive(true);
            _storyTextView.IntroView();
            _xrView.StartARSession();
        }

        public void SensingView()
        {
#if UNITY_EDITOR
            _sensingView.Setup(Texture2D.whiteTexture, _xrView.VFXCameraRT);
#else
            _sensingView.Setup(_xrView.ARCameraRT, _xrView.VFXCameraRT);
#endif
            //StartCoroutine(SensingViewEnumerator());
            _sensingView.SetActive(true);
            _sensingView.Show();
            _storyTextView.SensingView();
        }

        private IEnumerator SensingViewEnumerator()
        {
            yield return new WaitForSeconds(0.3f);
            _sensingView.SetActive(true);
            _sensingView.Show();
            _storyTextView.SensingView();
        }

        public void VisualizeView(DiagnoticsResult result)
        {
            _visualizeView.onEndVideo = () =>
            {
                _sensingView.Hide(() =>
                {
                    _xrView.InitVFXParams();
                    onNext?.Invoke(AppStatus.WAITING);
                });
            };
            var masterSeq = DOTween.Sequence();
            var seq1 = _sensingView.BeforeVisualize(result);
            var seq2 = _xrView.BeforeVisualize(result);
            _storyTextView.BeforeVisualizeView();
            masterSeq
            .Join(seq1)
            .Join(seq2)
            .InsertCallback(12f, () => _xrView.StopARSession())
            .InsertCallback(14f, () =>
            {
                _visualizeView.StartVideo(result);
                _visualizeView.StartGaugeAnimation();
                _storyTextView.VisualizeView(result);
            });
        }

    }
}
