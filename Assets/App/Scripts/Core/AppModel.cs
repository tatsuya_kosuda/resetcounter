﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace resetcounter
{
    public class AppModel
    {
        private ReactiveProperty<AppStatus> _appStatus;
        public IReadOnlyReactiveProperty<AppStatus> AppStatus
        {
            get
            {
                return _appStatus;
            }
        }
        public void SetAppStatus(AppStatus appStatus)
        {
            _appStatus.Value = appStatus;
        }

        private ReactiveProperty<bool> _isDetectedFace;
        public IReadOnlyReactiveProperty<bool> IsDetectedFace
        {
            get
            {
                return _isDetectedFace;
            }
        }
        public void SetIsDetectedFace(bool isDetectedFace)
        {
            _isDetectedFace.Value = isDetectedFace;
        }

        private DiagnoticsResult _result = DiagnoticsResult.NONE;
        public DiagnoticsResult Result
        {
            get
            {
                return _result;
            }
        }

        public DiagnoticsResult GetDiagnoticsResult()
        {
            if (_result == DiagnoticsResult.NONE)
            {
                _result = (DiagnoticsResult)Random.Range(1, 4);
            }
            else
            {
                var tmp = (DiagnoticsResult)Random.Range(1, 4);

                while (_result == tmp)
                {
                    tmp = (DiagnoticsResult)Random.Range(1, 4);
                }

                _result = tmp;
            }

            return _result;
        }

        public string TransactionId
        {
            get;
            private set;
        }

        public AppModel()
        {
            _appStatus = new ReactiveProperty<AppStatus>();
            _isDetectedFace = new ReactiveProperty<bool>(false);
        }

        public void Init()
        {
            _isDetectedFace.Value = false;
            TransactionId = GenerateRandomString();
        }

        private const string RANDOM_CHARS = "0123456789abcdefghijklmnopqrstuvwxyz";

        private string GenerateRandomString()
        {
            var sb = new System.Text.StringBuilder(Random.Range(25, 30));
            var r = new System.Random();

            for (int i = 0; i < sb.Capacity; i++)
            {
                int pos = r.Next(RANDOM_CHARS.Length);
                char c = RANDOM_CHARS[pos];
                sb.Append(c);
            }

            return sb.ToString();
        }
    }
}
