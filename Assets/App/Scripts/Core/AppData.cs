﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

namespace resetcounter
{
    public class LocalizeText
    {
        public string id;

        public string ja;
        public string en;
        public string zhcn; //簡体字
        public string zhtw; //繁体字
        public string th; // タイ

        public LocalizeText(string[] split)
        {
            id = split[0].Replace("\\n", "\n");
            ja = split[1].Replace("\\n", "\n");
            en = split[2].Replace("\\n", "\n");
            zhcn = split[3].Replace("\\n", "\n");
            zhtw = split[4].Replace("\\n", "\n");
            th = split[5].Replace("\\n", "\n");
        }

        public string GetText(Language lang)
        {
            switch (lang)
            {
                case Language.JA:
                    return ja;

                case Language.EN:
                    return en;

                case Language.ZHCN:
                    return zhcn;

                case Language.ZHTW:
                    return zhtw;

                case Language.TH:
                    return th;
            }

            return "";
        }
    }

    [System.Serializable]
    public class LocalizeTextSettings
    {
        public LocalizeTextSetting[] settings;
        public static readonly string PATH = Application.streamingAssetsPath + "/LocalizaTextSetting.json";
        public LocalizeTextSettings()
        {
            settings = new LocalizeTextSetting[1];
        }
    }

    [System.Serializable]
    public class LocalizeTextSetting
    {
        public string id;
        public float duration;
        public float[] characterSpacing;
        public LocalizeTextSetting()
        {
            id = "";
            duration = 0;
            characterSpacing = new float[5] { 0, 0, 0, 0, 0 };
        }
    }

    public struct LocalizeTextInfo
    {
        public string text;
        public float duration;
        public float[] characterSpacing;
    }

    public enum AppStatus
    {
        NONE,
        WAITING,
        INTRO,
        SENSING,
        VISUALIZE,
        OUTRO
    }

    public enum Language
    {
        JA,
        EN,
        ZHCN,
        ZHTW,
        TH
    }

    public enum DiagnoticsResult
    {
        ENE = 1,
        REF = 2,
        REL = 3,
        NONE
    }

    public class RequestData
    {
        public string region_code = PlayerPrefs.GetString("region_code");
        public string store_code = PlayerPrefs.GetString("store_code");
        public string app_id = "reset";
        public LogData[] log_data;

#if UNITY_EDITOR
        public static readonly string PATH = "Log/" + System.DateTime.Now.ToString("yyyyMMdd") + ".json";
#else
        public static readonly string PATH = Application.persistentDataPath + "/Log/" + System.DateTime.Now.ToString("yyyyMMdd") + ".json";
#endif
    }

    [System.Serializable]
    public class LogData
    {
        private static readonly string[] PAGE_CODE = new string[10]
        {
            "PG_RESET_002",
            "PG_RESET_011",
            "PG_RESET_021",
            "PG_RESET_022",
            "PG_RESET_031",
            "PG_RESET_041",
            "PG_RESET_051",
            "PG_RESET_061",
            "PG_RESET_099",
            "PG_RESET_098"
        };

        private static readonly string[] LANG_CODE = new string[5]
        {
            "japanese",
            "english",
            "simplified_chinese",
            "traditional_chinese",
            "thai"
        };

        public int action_type;
        public string contents_id = SystemInfo.deviceName;
        public string transaction_id = "";
        public string page_code = PAGE_CODE[0];
        public string language_code = LANG_CODE[0];
        public System.DateTime measure_datetime;
        public ResetLog reset_log;

        public LogData()
        {
        }

        public LogData(ActionType actionType, string transactionId, int pageId, Language lang)
        {
            action_type = (int) actionType;
            contents_id = SystemInfo.deviceName;
            transaction_id = transactionId;
            page_code = PAGE_CODE[pageId];
            language_code = LANG_CODE[(int) lang];
            measure_datetime = System.DateTime.UtcNow;
            reset_log = new ResetLog();
        }

        public LogData(ActionType actionType, string transactionId, int pageId, Language lang, DiagnoticsResult result)
        {
            action_type = (int) actionType;
            contents_id = SystemInfo.deviceName;
            transaction_id = transactionId;
            page_code = PAGE_CODE[pageId];
            language_code = LANG_CODE[(int) lang];
            measure_datetime = System.DateTime.UtcNow;
            reset_log = new ResetLog() { result_id = (int) result };
        }
    }

    [System.Serializable]
    public class ResetLog
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? result_id;
    }

    public enum ActionType
    {
        DEFAULT = 1,
        CHANGE_LANG = 7,
        DETECT_FACE = 17,
        TESTING = 14,
        END = 98
    }

    public class ResponseData
    {
        public int status;
        public string message;
        public System.DateTime cms_last_update;
        public System.DateTime server_datetime;
    }

}
