﻿using UnityEngine;
using System.Runtime.InteropServices;

public class UnityAromaShooter
{

    [DllImport("__Internal")]
    private static extern void open_connection_view();

    [DllImport("__Internal")]
    private static extern void diffuse(int duration);

    [DllImport("__Internal")]
    private static extern void auto_connect();

    public static void OpenConnectionView()
    {
#if UNITY_EDITOR
        Debug.Log("Open Connection View");
#else
        open_connection_view();
#endif
    }

    public static void Diffuse(int duration)
    {
#if UNITY_EDITOR
        Debug.Log($"diffuse: duration={duration}ms");
#else
        diffuse(duration);
#endif
    }

    public static void Connect()
    {
#if UNITY_EDITOR
        Debug.Log("auto connect");
#else
        auto_connect();
#endif
    }

}
