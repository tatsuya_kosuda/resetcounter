//
//  NativePlugin.h
//  PROJECT_NAME
//
//  Created by AUTHOR on YYYY/MM/DD.
//

#ifndef NativePlugin_h
#define NativePlugin_h

#ifdef __cplusplus
extern "C" {
#endif
int add_one(int num);

void open_connection_view();
void diffuse(int duration);
void auto_connect();

#ifdef __cplusplus
}
#endif

#endif /* NativePlugin_h */
