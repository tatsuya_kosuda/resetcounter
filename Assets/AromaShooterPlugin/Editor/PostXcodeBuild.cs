﻿using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

public class PostXcodeBuild
{
    [PostProcessBuild]
    public static void SetXcodePlist(BuildTarget buildTarget, string pathToBuiltProject)
    {
        if (buildTarget != BuildTarget.iOS) { return; }

        // bluetooth
        var plistPath = pathToBuiltProject + "/Info.plist";
        var plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(plistPath));
        var rootDict = plist.root;
        rootDict.SetString("NSBluetoothAlwaysUsageDescription", "Use for communicate with AromaShooter.");
        File.WriteAllText(plistPath, plist.WriteToString());
        // bit code
        string projectPath = PBXProject.GetPBXProjectPath(pathToBuiltProject);
        PBXProject pbxProject = new PBXProject();
        pbxProject.ReadFromFile(projectPath);
        var targetGuid = pbxProject.GetUnityMainTargetGuid();
        pbxProject.SetBuildProperty(targetGuid, "ENABLE_BITCODE", "NO");
        pbxProject.WriteToFile(projectPath);
        var projectInString = File.ReadAllText(projectPath);
        projectInString = projectInString.Replace("ENABLE_BITCODE = YES;", $"ENABLE_BITCODE = NO;");
        File.WriteAllText(projectPath, projectInString);
    }
}